#include "verifierAlphanum.h"
#include "convertirAccents.h"
#include "cesarC.h"
#include "cesarD.h"
#include "vigenereC.h"
#include "vigenereD.h"
#include "clearBuffer.h"
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <ctype.h>




void vigenereD(wchar_t chaine[], wchar_t cle[]) {
	int i = 0;
	int j = 0;
	while(chaine[i] != L'\0') {
		if(chaine[i] >= L'A' && chaine[i] <= L'Z') {
			wchar_t c = chaine[i] - L'A';
			c -= toupper(cle[j]) - L'A';
			if(c < 0) {
				chaine[i] = c + 1+ L'Z';
			}
			else {
				c = c % 26;
				chaine[i] = c + L'A';
			}
		}
		else if (chaine[i] >= L'a' && chaine[i] <= L'z') {
			wchar_t c = chaine[i] - L'a';
			c -= tolower(cle[j]) - L'a';
			if(c < 0) {
				chaine[i] = c + 1 + L'z';
			}
			else {
			c = c % 26;
			chaine[i] = c + L'a';
			}
		}
		i++;
		if (isalnum(chaine[i])) { // on incrémente j que si on rencontre un caractère alphanumérique
			j = (j+1) % wcslen(cle);
		}
	}
}	