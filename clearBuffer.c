#include "verifierAlphanum.h"
#include "convertirAccents.h"
#include "cesarC.h"
#include "cesarD.h"
#include "vigenereC.h"
#include "vigenereD.h"
#include "clearBuffer.h"
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <ctype.h>




void clearBuffer() {
	wchar_t c;
	c = getwchar();
	while (L'\n' != c) {
	c = getwchar();
	}
}