#include "verifierAlphanum.h"
#include "convertirAccents.h"
#include "cesarC.h"
#include "cesarD.h"
#include "vigenereC.h"
#include "vigenereD.h"
#include "clearBuffer.h"
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <ctype.h>




void convertirAccents(wchar_t *message) {
	wchar_t accents[55] = L"ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖØÙÛÜÝàáâãäåçèéêëìíîïñòóôõöøùúûüýÿ";
	wchar_t sansAcc[55] = L"AAAAAACEEEEIIIINOOOOOOUUUYaaaaaaceeeeiiiinoooooouuuuyy";
	
	for (int i = 0; i < wcslen(message); i++) {
		for (int j = 0; j < 54; j++) {
			if (message[i] == accents[j]) {
				message[i] = sansAcc[j];
			}
		}
	}
	wprintf(L"Message converti en alphanumerique !\n");
}