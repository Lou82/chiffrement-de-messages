#include "verifierAlphanum.h"
#include "convertirAccents.h"
#include "cesarC.h"
#include "cesarD.h"
#include "vigenereC.h"
#include "vigenereD.h"
#include "clearBuffer.h"
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <ctype.h>


void verifierAlphanum(wchar_t message[]){
	int alphanum = 1; //indique que le message est alphanum ou non
	int i = 0;

	while (i < wcslen(message)-1 && alphanum == 1) {
		if (message[i] < L'0' || (message[i] > L'9' && message[i] < L'A') || (message[i] > L'Z' && message[i] < L'a') || message[i] > L'z') {
			wprintf(L"Caractere special detecte...\n");
			alphanum = 0; // msg n'est pas alphanum
			convertirAccents(message);
		}
		i++;
	}
	if (alphanum == 1) {
		wprintf(L"Le message est bien alphanumerique, pret pour le chiffrement !\n");
	}
}
