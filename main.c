#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <ctype.h>
#include "verifierAlphanum.h"
#include "convertirAccents.h"
#include "cesarC.h"
#include "cesarD.h"
#include "vigenereC.h"
#include "vigenereD.h"
#include "clearBuffer.h"






void main() {
	
	struct lconv *loc;
	setlocale(LC_ALL, "");
	loc=localeconv();

	wchar_t chaine[256];
	wchar_t code;
	wprintf(L"Saisir le message à coder\n");
	fgetws(chaine, 256, stdin);
	verifierAlphanum(chaine);
	
	wprintf(L"Quel code desirez-vous ? Tapez c pour Cesar, v pour Vigenere\n");
	wscanf(L"%lc", &code);
	if (code == L'c') { // code cesar
		int cle = 0;
		wchar_t choix;
	
		wprintf(L"Bienvenue dans le code Cesar !\n");
		wprintf(L"Tapez c pour chiffrer un message ou d pour dechiffrer\n");
		clearBuffer();
		wscanf(L"%lc", &choix);
		wprintf(L"Saisir la cle de chiffrement\n");
		wscanf(L"%d", &cle);
		if (choix == L'c') { //chiffrement cesar
			cesarC(chaine, cle);
			wprintf(L"Message chiffre : %ls \n", chaine);
		}
		else if (choix == L'd') { // déchiffrement cesar
			cesarD(chaine, cle);
			wprintf(L"Message dechiffre : %ls \n", chaine);
		}
	}
	else if (code == L'v') { // code vigenere
		wchar_t cle[256];
		wchar_t choix;

		wprintf(L"Bienvenue dans le code Vigenere !\n");
		wprintf(L"Tapez c pour chiffrer un message ou d pour dechiffrer\n");
		clearBuffer();
		wscanf(L"%lc", &choix);
		wprintf(L"Saisir la cle de chiffrement\n");
		wscanf(L"%ls", cle); //cle ou &cle?
		if (choix == L'c') { //chiffrement vigenere
			vigenereC(chaine, cle);
			wprintf(L"Message chiffre : %ls \n", chaine);
		}
		else if (choix == L'd') { // déchiffrement vigenere
			vigenereD(chaine, cle);
			wprintf(L"Message dechiffre : %ls \n", chaine);
		}
	}





	
}



