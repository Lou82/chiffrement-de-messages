#include "verifierAlphanum.h"
#include "convertirAccents.h"
#include "cesarC.h"
#include "cesarD.h"
#include "vigenereC.h"
#include "vigenereD.h"
#include "clearBuffer.h"
#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <locale.h>
#include <ctype.h>



void cesarC(wchar_t chaine[], int cle) {
	int i = 0;
	while(chaine[i] != L'\0') {
		if(chaine[i] >= L'A' && chaine[i] <= L'Z') {
			wchar_t c = chaine[i] - L'A';
			c += cle;
			c = c % 26;
			chaine[i] = c + L'A';
		}
		else if (chaine[i] >= L'a' && chaine[i] <= L'z') {
			wchar_t c = chaine[i] - L'a';
			c += cle;
			c = c % 26; 
			chaine[i] = c + L'a';
		}
		i++;
	}	
}