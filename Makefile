GCC = gcc
SRC = $(wildcard *.c)
BINAIRES = $(patsubst %.c,%.o,${SRC})


all : main

main: ${BINAIRES}
#${GCC} verifierAlphanum.o convertirAccents.o cesarC.o cesarD.o vigenereC.o vigenereD.o clearBuffer.o main.c -o main
	${GCC} $^ -o $@

#tout les .o : tout les .c et .h 
%.o: %.c %.h
	${GCC} -c $<
	
clean:
	rm main
	rm *.o
